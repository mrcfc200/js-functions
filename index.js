//* JS FUNCTIONS

//! Iteración 1

//? Completa la función que tomando dos números como argumento devuelva el más largo.

function sum(numberOne, numberTwo) {
  if (numberOne > numberTwo) {
    console.log(numberOne);
  } else {
    console.log(numberTwo);
  }
}

//! Iteración 2

//? Completa la función que tomando un array de strings como argumento devuelva el más largo, en caso de que dos strings tenga la misma longitud deberá devolver el primero.

const avengers = [
  "Hulk",
  "Thor",
  "IronMan",
  "Captain A.",
  "Spiderman",
  "Captain M.",
];

function findLongestWord(param) {
  let arrayCounter = [];
  for (var i = 0; i < param.length; i++) {
    arrayCounter.push(param[i].length);
  }
  let maxNumber = Math.max.apply(Math, arrayCounter);
  console.log(avengers[arrayCounter.indexOf(10)]);
}

findLongestWord(avengers);

//! Iteración 3

//? Implemente la función denominada sumNumbers que toma un array de números como argumento y devuelve la suma de todos los números de la matriz.

const numbers = [1, 2, 3, 5, 45, 37, 58];

sumNumbers = (param) => {
  let totalSum = 0;

  numbers.forEach((num) => {
    totalSum += num;
  });
  console.log(totalSum);
};

sumNumbers(numbers);

//! Iteración 4

//? Calcular un promedio es una tarea extremadamente común. Puedes usar este array para probar tu función:

const numbers = [12, 21, 38, 5, 45, 37, 6];
const other = [4, 5, 12, 288];
debugger;
function average(list) {
  let totalSum = 0;
  for (var i = 0; i < list.length; i++) {
    totalSum += list[i];
  }
  console.log(totalSum / list.length);
}

average(numbers);
average(other);

//! Iteración 5

//? Crea una función que reciba por parámetro un array y cuando es un valor number lo sume y de lo contrario cuente la longitud del string y lo sume. Puedes usar este array para probar tu función:

const mixedElements = [6, 1, "Rayo", 1, "vallecano", "10", "upgrade", 8, "hub"];

function averageWord(param) {
  let counter = 0;
  param.forEach((element) => {
    if (typeof element === "string") {
      counter += element.length;
    } else {
      counter += element;
    }
  });
  console.log(counter);
}

averageWord(mixedElements);

//! Iteración 6

//? Crea una función que reciba por parámetro un array y compruebe si existen elementos duplicados, en caso que existan los elimina para retornar un array sin los elementos duplicados. Puedes usar este array para probar tu función:

const duplicates = [
  "sushi",
  "pizza",
  "burger",
  "potatoe",
  "pasta",
  "ice-cream",
  "pizza",
  "chicken",
  "onion rings",
  "pasta",
  "soda",
];

function removeDuplicates(param) {
  cleanArray = [];
  param.forEach((food) => {
    if (cleanArray.includes(food) == false) {
      cleanArray.push(food);
    }
  });
  console.log(cleanArray);
}

removeDuplicates(duplicates);

//! Iteración 7

//?Crea una función que reciba por parámetro un array y el valor que desea comprobar que existe dentro de dicho array - comprueba si existe el elemento, en caso que existan nos devuelve un true y la posición de dicho elemento y por la contra un false. Puedes usar este array para probar tu función:

const nameFinder = [
  "Peter",
  "Steve",
  "Tony",
  "Natasha",
  "Clint",
  "Logan",
  "Xabier",
  "Bruce",
  "Peggy",
  "Jessica",
  "Marc",
];

finderName = (array, value) => {
  if (array.includes(value)) {
    console.log(true, ",position " + array.indexOf(value));
  } else {
    console.log("false");
  }
};

//! Iteración 8

//? Crea una función que nos devuelva el número de veces que se repite cada una de las palabras que lo conforma.  Puedes usar este array para probar tu función:

const counterWords = [
  "code",
  "repeat",
  "eat",
  "sleep",
  "code",
  "enjoy",
  "sleep",
  "code",
  "enjoy",
  "upgrade",
  "code",
];

repeatCounter = (array) => {
  newArray = [];
  array.forEach((element) => {
    if (newArray.includes(element) == false) {
      newArray.push(element);
    }
  });
  counterArray = [];
  newArray.forEach((newElement) => {
    counter = 0;

    for (var i = 0; i <= array.length; i++) {
      if (newElement == array[i]) {
        counter += 1;
      }
      if (i == array.length - 1) {
        counterArray.push(newElement + ": " + counter);
      }
    }
  });
  console.log(counterArray);
};

repeatCounter(counterWords);
